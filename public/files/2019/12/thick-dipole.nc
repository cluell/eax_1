model ("thick-dipole") {

real height, total_len, ant_len, ant_height, gauge, center_space;
element top_left, top_right, bottom_left, bottom_right, left_top, left_bottom, right_top, right_bottom, center;

height = 1;
ant_height= 0.03;
center_space = 0.02;
total_len = 1.96;
ant_len = total_len/2 - ant_height;
gauge = #4;

top_left = wire(0, -ant_len/2, height+ant_height,0, -center_space/2, height+ant_height, gauge, 11);
top_right = wire(0, center_space/2, height+ant_height, 0, ant_len/2, height+ant_height, gauge, 11);
bottom_left = wire(0, -ant_len/2, height, 0, -center_space/2, height, gauge, 11);
bottom_right = wire(0, center_space/2, height, 0, ant_len/2, height, gauge, 11);
left_top = wire(0, -center_space/2, height+ant_height, 0, -center_space/2, height+ ant_height/2, gauge, 3);
left_bottom = wire(0, -center_space/2, height+ant_height/2, 0, -center_space/2, height, gauge, 3);
right_top = wire(0, center_space/2, height+ant_height, 0, center_space/2, height+ant_height/2, gauge, 3);
right_bottom = wire(0, center_space/2, height+ant_height/2, 0, center_space/2, height, gauge, 3);
center = wire(0, -center_space/2, height+ant_height/2, 0, center_space/2, height+ant_height/2, gauge, 3);
voltageFeed(center, 1.0, 0.0);

azimuthPlotForElevationAngle(15);
elevationPlotForAzimuthAngle(0);

setFrequency(145.8);
// frequencySweep(136, 158, 20); // SWR < 2

freespace();
// averageGround();

} 
