model ("folded-dipole") {

real height, total_len, ant_len, ant_height, gauge;
element top, bottom, left, right;

height = 1;
ant_height= 0.025;
total_len = 1.9425;
ant_len = total_len/2 - ant_height;
gauge = #4;

top = wire(0, -ant_len/2, height+ant_height, 0, ant_len/2, height+ant_height, gauge, 21);
left = wire(0, -ant_len/2, height+ant_height, 0, -ant_len/2, height, gauge, 3);
right = wire(0, ant_len/2, height+ant_height, 0, ant_len/2, height, gauge, 3);
bottom = wire(0, -ant_len/2, height, 0, ant_len/2, height, gauge, 21);

voltageFeed(bottom, 1.0, 0.0);

azimuthPlotForElevationAngle(15);
elevationPlotForAzimuthAngle(0);

setFrequency(145);
// frequencySweep(134.5, 159, 10); // SWR < 2

freespace();
// averageGround();

} 
