reading file "./heaptrack.test_rbtree.22023.gz" - please wait, this might take some time...
Debuggee command was: ./test_rbtree
finished reading file, now analyzing data:

MOST CALLS TO ALLOCATION FUNCTIONS
31111 calls to allocation functions with 4.98MB peak consumption from
tree_allocfunc
  at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:37
  in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
7777 calls with 1.24MB peak consumption from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    left_right_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:166
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:430
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
7777 calls with 1.24MB peak consumption from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    right_left_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:247
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:431
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
7777 calls with 1.24MB peak consumption from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    direct_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:327
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:432
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
7777 calls with 1.24MB peak consumption from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    inverted_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:400
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:433
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 calls with 160B peak consumption from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    general_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:67
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:429
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
  and 2 from 2 other places

1 calls to allocation functions with 72.70KB peak consumption from
_GLOBAL__sub_I_eh_alloc.cc
  at /build/gcc/src/gcc/libstdc++-v3/libsupc++/eh_alloc.cc:326
  in /usr/lib/libstdc++.so.6
1 calls with 72.70KB peak consumption from:
    call_init.part.0
      in /lib64/ld-linux-x86-64.so.2
    _dl_init
      in /lib64/ld-linux-x86-64.so.2
    0x7ff8612ecda9
      in /lib64/ld-linux-x86-64.so.2


PEAK MEMORY CONSUMERS

WARNING - the data below is not an accurate calcuation of the total peak consumption and can easily be wrong.
 For an accurate overview, disable backtrace merging.
4.98MB peak memory consumed over 31111 calls from
tree_allocfunc
  at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:37
  in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1.24MB consumed over 7777 calls from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    left_right_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:166
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:430
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1.24MB consumed over 7777 calls from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    right_left_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:247
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:431
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1.24MB consumed over 7777 calls from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    direct_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:327
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:432
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1.24MB consumed over 7777 calls from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    inverted_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:400
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:433
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
160B consumed over 1 calls from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    general_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:67
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:429
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
  and 320B from 2 other places

72.70KB peak memory consumed over 1 calls from
_GLOBAL__sub_I_eh_alloc.cc
  at /build/gcc/src/gcc/libstdc++-v3/libsupc++/eh_alloc.cc:326
  in /usr/lib/libstdc++.so.6
72.70KB consumed over 1 calls from:
    call_init.part.0
      in /lib64/ld-linux-x86-64.so.2
    _dl_init
      in /lib64/ld-linux-x86-64.so.2
    0x7ff8612ecda9
      in /lib64/ld-linux-x86-64.so.2

MEMORY LEAKS
72.70KB leaked over 1 calls from
_GLOBAL__sub_I_eh_alloc.cc
  at /build/gcc/src/gcc/libstdc++-v3/libsupc++/eh_alloc.cc:326
  in /usr/lib/libstdc++.so.6
72.70KB leaked over 1 calls from:
    call_init.part.0
      in /lib64/ld-linux-x86-64.so.2
    _dl_init
      in /lib64/ld-linux-x86-64.so.2
    0x7ff8612ecda9
      in /lib64/ld-linux-x86-64.so.2


MOST TEMPORARY ALLOCATIONS
5 temporary allocations of 31111 allocations in total (0.02%) from
tree_allocfunc
  at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:37
  in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 temporary allocations of 7777 allocations in total (0.01%) from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    left_right_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:166
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:430
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 temporary allocations of 7777 allocations in total (0.01%) from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    right_left_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:247
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:431
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 temporary allocations of 7777 allocations in total (0.01%) from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    direct_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:327
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:432
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 temporary allocations of 7777 allocations in total (0.01%) from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    inverted_walk_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:400
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:433
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
1 temporary allocations of 1 allocations in total (100.00%) from:
    rb_insert
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    general_test
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:76
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    run_tests
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:429
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
    main
      at /home/eax/projects/c/c-algorithms/test/struct/test_rbtree.c:458
      in /home/eax/projects/c/c-algorithms/build/test/struct/test_rbtree
  and 0B from 2 other places


total runtime: 0.28s.
bytes allocated in total (ignoring deallocations): 5.05MB (18.23MB/s)
calls to allocation functions: 31112 (112317/s)
temporary memory allocations: 5 (18/s)
peak heap memory consumption: 1.32MB
peak RSS (including heaptrack overhead): 9.80MB
total memory leaked: 72.70KB
